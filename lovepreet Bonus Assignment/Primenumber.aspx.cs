﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace lovepreet_Bonus_Assignment
{
    public partial class primenumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
       
        protected void Check_PrimeNumber(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            

            int enterInput = int.Parse(InputNumber.Text);

            bool flag = false;

            for (int i = enterInput - 1; i > 1; i--)
            {
                if (enterInput % i == 0)
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
               output.InnerHtml = "<strong>output:</strong> \"" + enterInput + "\" is a Prime Number";
            }
            else
            {
               output.InnerHtml = "<strong>output:</strong> \"" + enterInput + "\" is not a Prime Number";
            }
        }

    }
}