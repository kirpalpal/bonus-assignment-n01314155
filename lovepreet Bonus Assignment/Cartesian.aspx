﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cartesian.aspx.cs" Inherits="lovepreet_Bonus_Assignment.cartesian_axis" %>
<asp:Content ID="Cont1" ContentPlaceHolderID="Cont1" runat="server">
    <div>
         <br />
        <br />
        Enter the number for x-axis and y-axis to check the pair of that number lies in which quadrant. 
        <br />
        <br />
        </div>
</asp:Content>
<asp:Content ID="userInputControls" ContentPlaceHolderID="cont2" runat="server">
    <div>
        <asp:Label runat="server">enter value here for x-axis: </asp:Label>
        <!-- Enter the value to check the quadrant and here enter the value for the x-axis here and required,regular expression and custom validator applied with it-->
        <asp:TextBox runat="server" ID="XAxis"></asp:TextBox>

        <asp:RequiredFieldValidator ID="XAxisRequired" runat="server" ControlToValidate="XAxis" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="XAxisRegularExpression" runat="server" ControlToValidate="XAxis" ErrorMessage="Please enter a valid number" ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <asp:CustomValidator ID="InputXAxisCustomValidator" runat="server" ControlToValidate="XAxis" OnServerValidate="XAxisCustomValidator_ServerValidate" ErrorMessage="The value must be a non-zero number." ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
        <br />
        <!--Enter the value to check the quadrant and here enter the value for the y-axis here and required,regular expression and custom validator applied with it-->
        <asp:Label runat="server">Enter value here for y-axis: </asp:Label>
        <asp:TextBox runat="server" ID="YAxis"></asp:TextBox>
        <asp:RequiredFieldValidator ID="YAxisRequired" runat="server" ControlToValidate="YAxis" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="YAxisRegularExpression" runat="server" ControlToValidate="YAxis" ErrorMessage="Please enter a valid number to proceed." ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <asp:CustomValidator ID="YAxisCustomValidator" runat="server" ControlToValidate="YAxis" OnServerValidate="YAxisCustomValidator_ServerValidate" ErrorMessage="Y-Axis value must be a non-zero number." ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
        <br />

        <br />

        <!-- button to check the output after inserting the value -->
        <asp:Button ID="submitNumber" Text="check here to see the quadrant" runat="server" OnClick="Quadrant_value"/>
    </div>
</asp:Content>
<asp:Content ID="resultArea" ContentPlaceHolderID="Cont3" runat="server">
    <div id="output" runat="server"></div>
</asp:Content>




