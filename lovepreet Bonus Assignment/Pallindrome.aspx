﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pallindrome.aspx.cs" Inherits="lovepreet_Bonus_Assignment.pallindrome" %>
<asp:Content ID="Cont1" ContentPlaceHolderID="Cont1" runat="server">
    <div> 

        <br />
        <br />
         give number which is palindrome.
        <br />
        <br />
        <br/>
    </div>

</asp:Content>
<asp:Content ID="userInputControls" ContentPlaceHolderID="Cont2" runat="server">
    <div>

        <asp:Label runat="server">Input string here! </asp:Label>

        <br/>

        <!-- Enter the string  to check weather the string is palindrome or not--> 
        <asp:TextBox runat="server" ID="InputString"></asp:TextBox>
        <asp:RequiredFieldValidator ID="InputStringRequired" runat="server" ControlToValidate="InputString" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="InputStringRegularExpression" runat="server" ControlToValidate="InputString" ErrorMessage="Please enter a valid string to proceed." ValidationExpression="[a-zA-Z ]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />

        <!-- Button to check the result after inserting the value--> 

        <asp:Button ID="submitInputString" Text="Test string is palindrome" runat="server" OnClick="Check_Palindrome"/>

    </div>

</asp:Content>
<asp:Content ID="resultArea" ContentPlaceHolderID="Cont3" runat="server">
    <div id="output" runat="server"></div>
</asp:Content>
