﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace lovepreet_Bonus_Assignment
{
    public partial class pallindrome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Check_Palindrome(object sender, EventArgs e)
        {

            //first check to make sure everything is valid
            if (!Page.IsValid)
            {
                return;
            }

            string userInput = InputString.Text;

            string userInputInLowerCase = userInput.ToLower();
            string userInputWithoutSpaces = userInputInLowerCase.Replace(" ", string.Empty);
            string userInputInReverse = string.Empty;
            

            int length = userInputWithoutSpaces.Length;
            bool flag = false;
            for (int i = 0; i < length / 2; i++)
            {
                if (userInputWithoutSpaces[i] != userInputWithoutSpaces[length - i - 1])
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
               output.InnerHtml = "<strong>Result:</strong> \"" + userInput + "\" is a Palindrome String";
            }
            else
            {
               output.InnerHtml = "<strong>Result:</strong> \"" + userInput + "\" is not a Palindrome String";
            }
        }
    }
}