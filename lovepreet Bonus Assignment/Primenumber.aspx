﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Primenumber.aspx.cs" Inherits="lovepreet_Bonus_Assignment.primenumber" %>
<asp:Content ID="Cont1" ContentPlaceHolderID="Cont1" runat="server">
    <div>

        <br />
        <br />

        Enter positive number is it prime or not.
        
        <br />
        <br />
    </div>

</asp:Content>

<asp:Content ID="userInputControls" ContentPlaceHolderID="Cont2" runat="server">
    <div>

  <!-- Enter the number for checking wheather it is prime or not by applying Requiredfield and regular Expression validator -->
        <asp:Label runat="server"> check number for Prime Number. </asp:Label>
        <asp:TextBox runat="server" ID="InputNumber"></asp:TextBox>
        <asp:RequiredFieldValidator ID="InputNumberRequired" runat="server" ControlToValidate="InputNumber" ErrorMessage="Value required." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="InputNumberRegularExpression" runat="server" ControlToValidate="InputNumber" ErrorMessage="Please enter a valid number to proceed." ValidationExpression="[0-9]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        <!-- button to check the result after inserting the number-->
        
        <asp:Button ID="submitInputNumber" Text="Check for prime number here!" runat="server" OnClick="Check_PrimeNumber"/>
    </div>
    <br/>
    <br/>
</asp:Content>
<asp:Content ID="resultArea" ContentPlaceHolderID="Cont3" runat="server">
    <div id="output" runat="server"></div>
</asp:Content>
